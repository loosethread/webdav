﻿open System
open System.Collections.Generic
open System.Web.Http
open System.Threading.Tasks
open Owin
open Microsoft.Owin
open Microsoft.Owin.Hosting

open tdav

type Startup() =
    member self.Configuration (app:IAppBuilder) = 
                            let h = fun (z:IOwinContext) -> Async.StartAsTask(RequestHandler.ProcessRequest z ) :> Task
                            app.Run( System.Func<IOwinContext, Task> (h))
                            ()

[<EntryPoint>]
let main argv = 
    printfn "Started" 
    use s=WebApp.Start<Startup>("http://localhost:12345")
    System.Console.ReadLine() |>ignore
    0
